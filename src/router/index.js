/* eslint-disable import/first */
import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const authLogin = () => {
  let token = sessionStorage.getItem("token");
  return token !== undefined && token !== null;
};

const DefaultContainer = () => import("@/containers/DefaultContainer");

const Login = () => import("@/views/Login");

const Dashboard = () => import("@/views/Dashboard");
const CompanyProfile = () => import("@/views/CompanyProfile");

//administration

const Brodcast = () => import("@/views/administration/Brodcast");
const Kesatuan = () => import("@/views/administration/Kesatuan");
const News = () => import("@/views/administration/News");
const Status = () => import("@/views/administration/Status");
const UserRole = () => import("@/views/administration/UserRole");
const Users = () => import("@/views/administration/Users");
const UsersLevel = () => import("@/views/administration/UsersLevel");
const WorkflowStatus = () => import("@/views/administration/WorkflowStatus");

// reports
const SummaryByCategory = () => import("@/views/reports/SummaryByCategory");
const SummaryByDate = () => import("@/views/reports/SummaryByDate");
const SummaryByUser = () => import("@/views/reports/SummaryByUser");
const SummaryOffice = () => import("@/views/reports/SummaryOffice");
const SummaryStatus = () => import("@/views/reports/SummaryStatus");
const TopTenPetugas = () => import("@/views/reports/TopTenPetugas");

//pelaporan
const WorkOrder = () => import("@/views/pelaporan/WorkOrder");
const ReportDetails = () => import("@/views/pelaporan/ReportDetail");

const router = new Router({
  mode: "hash",
  linkActiveClass: "open active",
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: "/",
      redirect: "/dashboard",
      name: "Dashboard",
      component: DefaultContainer,
      meta: {
        auth: true
      },
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
          meta: {
            auth: true
          }
        },
        {
          path: "company-profile",
          name: "company-profile",
          component: CompanyProfile,
          meta: {
            auth: true
          }
        },
        {
          path: "administration",
          name: "Administration",
          component: {
            render(c) {
              return c("router-view");
            }
          },
          meta: {
            auth: true
          },
          children: [
            {
              path: "news",
              name: "News",
              component: News,
              meta: {
                auth: true
              }
            },
            {
              path: "broadcast",
              name: "Broadcast",
              component: Brodcast,
              meta: {
                auth: true
              }
            },
            {
              path: "kesatuan",
              name: "Kesatuan",
              component: Kesatuan,
              meta: {
                auth: true
              }
            },
            {
              path: "status",
              name: "Status",
              component: Status,
              meta: {
                auth: true
              }
            },
            {
              path: "user-role",
              name: "User Role",
              component: UserRole,
              meta: {
                auth: true
              }
            },
            {
              path: "users",
              name: "Users",
              component: Users,
              meta: {
                auth: true
              }
            },
            {
              path: "user-level",
              name: "User Level",
              component: UsersLevel,
              meta: {
                auth: true
              }
            },
            {
              path: "workflow-status",
              name: "Workflow Status",
              component: WorkflowStatus,
              meta: {
                auth: true
              }
            }
          ]
        },
        {
          path: "work",
          name: "Work",
          component: {
            render(c) {
              return c("router-view");
            }
          },
          meta: {
            auth: true
          },
          children: [
            {
              path: "",
              name: "Pelaporan",
              component: WorkOrder,
              meta: {
                auth: true
              }
            },
            {
              path: ":ticket_id",
              name: "report-details",
              component: ReportDetails,
              meta: {
                auth: true
              }
            }
          ]
        },
        {
          path: "reports",
          name: "Reports",
          component: {
            render(c) {
              return c("router-view");
            }
          },
          meta: {
            auth: true
          },
          children: [
            {
              path: "summary-by-category",
              name: "Summary By Category",
              component: SummaryByCategory,
              meta: {
                auth: true
              }
            },
            {
              path: "summary-by-date",
              name: "Summary By Date",
              component: SummaryByDate,
              meta: {
                auth: true
              }
            },
            {
              path: "summary-by-user",
              name: "Summary By User",
              component: SummaryByUser,
              meta: {
                auth: true
              }
            },
            {
              path: "summary-office",
              name: "Summary Office",
              component: SummaryOffice,
              meta: {
                auth: true
              }
            },
            {
              path: "summary-status",
              name: "Summary Status",
              component: SummaryStatus,
              meta: {
                auth: true
              }
            },
            {
              path: "top-ten-petugas",
              name: "Top Ten Petugas",
              component: TopTenPetugas,
              meta: {
                auth: true
              }
            }
          ]
        }
      ]
    },
    {
      path: "/auth/login",
      name: "login",
      component: Login
    },
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    if (!authLogin()) {
      //   next({
      //     path: "/auth/login",
      //     query: { redirect: to.fullPath }
      //   });
      next();
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
