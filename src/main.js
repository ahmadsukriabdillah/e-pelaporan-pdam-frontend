import Vue from 'vue'
import { VueAxios } from "./utils/request";
import App from './App.vue'
import FishUi from 'fish-ui'
import Storage from 'vue-ls';
import router from './router';
let options = {
  namespace: 'e_pelaporan__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local', // storage name session, local, memory
};
Vue.use(VueAxios)
Vue.use(Storage, options);
Vue.use(FishUi);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
