export default [
  {
    key: '1',
    name: "Dashboard",
    title: "Dashboard",
    url: "/dashboard",
    icon: "fa fa-bolt"
  },
  {
    key: '2',
    name: "Company Profile",
    title: "Company Profile",
    url: "/company-profile",
    icon: "fa fa-bolt"
  },
  {
    key: '3',
    name: "Administration",
    title: "Administration",
    url: "/administration",
    icon: "fa fa-bolt",
    children: [
      {
        key: '1',
        name: "News",
        title: "News",
        url: "/news",
        icon: "fa fa-bolt"
      },
      {
        key:'2',
        name: "Blasting",
        title: "Blasiting",
        url: "/broadcast",
        icon: "fa fa-bolt"
      },
      {
        key:'4',
        name: "Kesatuan",
        title: "Kesatuan",
        url: "/kesatuan",
        icon: "fa fa-bolt"
      },
      {
        key:'5',
        name: "User Leve",
        title: "Kesatuan",
        url: "/user-level",
        icon: "fa fa-bolt"
      },
      {
        key:'6',
        name: "User",
        title: "User",
        url: "/user",
        icon: "fa fa-bolt"
      },
      {
        key:'7',
        name: "User Level Notification",
        title: "User Level Notification",
        url: "/user-notification",
        icon: "fa fa-bolt"
      },
      {
        key:'8',
        name: "Status",
        title: "Status",
        url: "/status",
        icon: "fa fa-bolt"
      },
      {
        key:'9',
        name: "Workflow Status",
        title: "Workflow Status",
        url: "/workflow-status",
        icon: "fa fa-bolt"
      },
      {
        key:'10',
        name: "User Status Role",
        title: "User Status Role",
        url: "/user-status-role",
        icon: "fa fa-bolt"
      }      
    ]
  },
  {
    key: '4',
    name: "Pengaduan",
    title: "Pengaduan",
    url: "/work",
    icon: "fa fa-bolt"
  },
  {
    key: '5',
    name: "Report",
    title: "Report",
    url: "/reports",
    icon: "fa fa-bolt",
    children: [
      {
        key:'1',
        name: "Summary Status",
        title: "Summary Status",
        url: "/summary-status",
        icon: "fa fa-bolt"
      },
      {
        key:'2',
        name: "Summary Office",
        title: "Summary Office",
        url: "/summary-office",
        icon: "fa fa-bolt"
      },
      {
        key:'3',
        name: "Summary By Date",
        title: "Summary By Date",
        url: "/summary-by-date",
        icon: "fa fa-bolt"
      },
      {
        key:'4',
        name: "Summary By User",
        title: "Summary By User",
        url: "/summary-by-user",
        icon: "fa fa-bolt"
      },
      {
        key:'5',
        name: "Summary By Category",
        title: "Summary By Category",
        url: "/summary-by-category",
        icon: "fa fa-bolt"
      },
      {
        key:'6',
        name: "Top 10 Petugas",
        title: "Top 10 Petugas",
        url: "/top-ten-petugas",
        icon: "fa fa-bolt"
      },
    ]
  },

];
